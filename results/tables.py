"""
Functions for the AWS Serverless Application Model that are
used to display the results.
"""

# Copyright (C) 2021  Paul Suh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import boto3
import mako.template


# global resources that are initialized once at load time, so that
# they don't need to be initialized for each event
dynamodb = boto3.resource('dynamodb')
weekday_summary_table = dynamodb.Table("net.mspex.ElmStDogParkSurvey.VisitsSummaryWeekday")
weekend_summary_table = dynamodb.Table("net.mspex.ElmStDogParkSurvey.VisitsSummaryWeekend")

with open("results_template.html") as html_template_file:
    html_source = html_template_file.read()
html_template = mako.template.Template(html_source)


def twentyfour_to_twelve(twentyfour_time: str) -> str:
    """
    Convert a 24-hour time (e.g., 1420) to a 12-hour time (2:20 PM)

    :param twentyfour_time: 24-hour time string
    :return: 12-hour time string
    """
    if twentyfour_time.startswith('12'):
        # have to special case 12:XX PM
        return str(int(twentyfour_time[0:2])) + ':' + twentyfour_time[2:] + ' PM'
    elif twentyfour_time.startswith((
        '13',
        '14',
        '15',
        '16',
        '17',
        '18',
        '19',
        '20',
        '21',
        '22',
        '23',
    )):
        return str(int(twentyfour_time[0:2])-12) + ':' + twentyfour_time[2:] + ' PM'
    else:
        return str(int(twentyfour_time[0:2])) + ':' + twentyfour_time[2:] + ' AM'


def transform_item(one_item: dict) -> dict:
    """
    Utility function used as a part of the list comprehensions
    :param one_item: a counting slot with a 24-hour time
    :return: a counting slot with a 12-hour time
    """
    # change the item to 12-hour time
    return {
        'time2400': twentyfour_to_twelve(one_item['time2400']),
        'visits': one_item['visits']
    }


def retrieve_visit_counts(event: dict, context: object) -> dict:
    """
    Retrieve the summary tables and put them into an HTML page

    :param event: dict containing info about the GET request from the API gateway
    :param context: object containing info about the execution environment from AWS
    :return: a dict containing an HTTP status code, custom headers, and the HTML body
    """

    # get everything from the summary tables
    weekday_response = weekday_summary_table.scan()
    weekend_response = weekend_summary_table.scan()

    # transform to 12-hour time
    # there's prob a string formatting way to do this
    total_weekday_visits = sum([t['visits'] for t in weekday_response['Items']])
    total_weekend_visits = sum([t['visits'] for t in weekend_response['Items']])

    html_response = html_template.render(
        weekday_hours_list=[transform_item(i) for i in weekday_response['Items']],
        weekend_hours_list=[transform_item(i) for i in weekend_response['Items']],
        weekday_visits=total_weekday_visits,
        weekend_visits=total_weekend_visits
    )

    return {
        "statusCode": 200,
        "headers": {"Content-Type": "text/html"},
        "body": html_response
    }


def redirect_to_combined_page(event: dict, context: object) -> dict:
    """
    Return a 301 redirect to the new summary page

    :param event: dict containing info about the GET request from the API gateway
    :param context: object containing info about the execution environment from AWS
    :return: a dict containing an HTTP status code, custom headers, and the HTML body
    """

    return {
        "statusCode": 301,
        "headers": {
            "Content-Type": "text/html",
            "Location": "https://elm-st-dog-park-survey.mspex.net/visit-counts"
        },
        "body":
        """
        <html>
            <head>
                <title>301 Moved Permanently</title>
            </head>
            <body>
                <p>Moved to <a href='https://elm-st-dog-park-survey.mspex.net/visit-counts'>
                    https://elm-st-dog-park-survey.mspex.net/visit-counts</a></p>
            </body>
        </html>
        """
    }


def generate_csv(event: dict, context: object) -> dict:
    """
    Return a 301 redirect to the new summary page

    :param event: dict containing info about the GET request from the API gateway
    :param context: object containing info about the execution environment from AWS
    :return: a dict containing an HTTP status code, custom headers, and the CSV body
    """

    weekday_items = weekday_summary_table.scan()["Items"]
    weekend_items = weekend_summary_table.scan()["Items"]

    weekday_string = "\n".join([f"weekday,{i['time2400']},{i['visits']}" for i in weekday_items])
    weekend_string = "\n".join([f"weekend,{i['time2400']},{i['visits']}" for i in weekend_items])

    return {
        "statusCode": 200,
        "headers": {
            "Content-Type": "text/csv",
            "Content-Disposition": "attachment; filename=elm-st-dog-park-visit-summary-counts.csv"
        },
        "body": weekday_string + "\n" + weekend_string
    }
