"""
Pre-populate the summary tables with zeros for each time slot
"""

import boto3

dynamodb = boto3.resource('dynamodb')
weekday_summary_table = dynamodb.Table("net.mspex.ElmStDogParkSurvey.VisitsSummaryWeekday")
weekend_summary_table = dynamodb.Table("net.mspex.ElmStDogParkSurvey.VisitsSummaryWeekend")


def prepopulate_table(t):
    """
    Pre-populate the table t from 0000 to 2350 hours with zero visit counts
    :param t: DynamoDB table ref
    :return: None
    """
    # check first to see that the table is empty
    result = t.scan()
    if len(result["Items"]) == 0:

        # fill in 10-minute intervals with zeroes
        for t2400 in [f"{h:02d}{m:02d}" for h in range(24) for m in range(0, 60, 10)]:
            t.put_item( Item={
                "id": "generic",
                "time2400": t2400,
                "visits": 0
            })


prepopulate_table(weekday_summary_table)
prepopulate_table(weekend_summary_table)
