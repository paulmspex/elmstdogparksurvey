# ElmStDogParkSurvey

I wanted to do a survey of what times people were bringing their dogs to the little pop-up dog park. I also wanted it to be: 

- As privacy-friendly as possible, so only record the time and no other information This ruled out most pre-built solutions.  
- Along with this, I wanted it to be as simple and easy to use as possible, so no login required and just scan a QR code to check in.  
- I wanted to use AWS Lambda and DynamoDB, partly as a learning exercise, partly because (for the small amount of expected usage) it would fall into the completely free tier. 

I had to adjust to the DynamoDB idiom rather than the more conventional relational DB idiom that I'm more accustomed to. In particular, filtering using an expression that is not an exact match for a key is **expensive**. To present the data, I wanted to divide it up into 10 minute intervals and show the counts on a web page. 

With an RDB I would just record times in a single table, then do a SQL call to to get visit counts by 10 minute intervals. The RDB would return an array of counts after about 500 msec, rendering the page and other overhead would take another 200 msec, and everything would be good.

With DynamoDB I had to make a separate call for each time interval. Each call using an indexed query was taking maybe 50 msec, and to build a page reguired (5 AM to 8 PM) 15*6=90 calls, totalling 4500 msec, or 4.5 seconds. Since the default timeout is 3 seconds, this isn't going to work. 

Instead, I took advantage of feature of DynamoDB and Lambda. You can set a trigger so that every time a DynamoDB table is changed, a specific Lambda function is called. I created another DynamoDB table that held 144 items, summarizing the visits for each 10 minute time slot over a 24 hour period. (Actually it was two tables, one for weekdays and another for weekends, but I digress.) I then set up a trigger on the first table that would call a Lambda function that incremented the visit count in the correct time slot when a person checked in and the timestamp was written to the first table. 

Creating the summary page requires a single call that grabs all of the data from the summary DynanmoDB table in one call and then puts in into a web page. 

In retrospect I could have placed the summary-incrementing call directly into the function that recorded the check-in timestamp, or even not recorded the timestamp at all. However, it was interesting to experiment with the Lambda trigger capability and having the actual timestamps would allow for additional data analysis later on.  