"""
Functions for the AWS Serverless Application Model that are
triggered when a user scans the QR code at the dog park.
"""

# Copyright (C) 2021  Paul Suh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import boto3
from boto3.dynamodb.conditions import Attr
from dynamodb_json import json_util
import datetime
import dateutil.tz
import mako.template


# global resources that are initialized once at load time, so that
# they don't need to be initialized for each event
dynamodb = boto3.resource('dynamodb')
visits_table = dynamodb.Table('net.mspex.ElmStDogParkSurvey.VisitsRegister')
weekday_summary_table = dynamodb.Table("net.mspex.ElmStDogParkSurvey.VisitsSummaryWeekday")
weekend_summary_table = dynamodb.Table("net.mspex.ElmStDogParkSurvey.VisitsSummaryWeekend")

with open("registration_template.html") as html_template_file:
    html_source = html_template_file.read()
html_template = mako.template.Template(html_source)


def register_visit(event: dict, context: object) -> dict:
    """
    Triggered by a scan of the QR code posted at the dog park gate.

    :param event: dict containing info about the GET request from the API gateway
    :param context: object containing info about the execution environment from AWS
    :return: a dict containing an HTTP status code, custom headers, and the HTML body
    """

    # get current time in US/Eastern time zone
    now_datetime = datetime.datetime.now(dateutil.tz.gettz('US/Eastern'))
    now_milliseconds: int = int(round(now_datetime.timestamp() * 1000))

    input_data = {
            'id': now_milliseconds,
            'year': now_datetime.year,
            'month': now_datetime.month,
            'day': now_datetime.day,
            'hour': now_datetime.hour,
            'minute': now_datetime.minute,
            'weekday': now_datetime.weekday()
    }
    db_response = visits_table.put_item(
        Item=input_data
    )

    html_response = html_template.render(
        datetime_string=f"{now_datetime:%B %d, %Y %I:%M %p}"
    )

    return {
        "statusCode": 200,
        "headers": {"Content-Type": "text/html"},
        "body": html_response
    }


def update_summary_table(event: dict, context: object) -> None:
    """
    Triggered by a change to the table that is .

    :param event: dict containing info about the stream from the DynamoDB update
    :param context: object containing info about the execution environment from AWS
    :return: None
    """
    for record_json in event['Records']:

        try:
            # wrap whole thing in a try-except block so that we can clear junk events
            # and just log them

            # convert to Python object for ease of use
            record = json_util.loads(record_json)
            # check to see if this is a weekday or a weekend
            # print(record)
            if record['dynamodb']['NewImage']['weekday'] in range(5):
                summary_table = weekday_summary_table
            else:
                summary_table = weekend_summary_table

            # generate the time2400 value from the values, with minutes rounded down
            # to the even ten minute interval
            minute_rounded = int(record['dynamodb']['NewImage']['minute'] / 10) * 10
            time2400_value = f"{record['dynamodb']['NewImage']['hour']:02d}{minute_rounded:02d}"

            # retrieve the summary item and update it
            current_item_json = summary_table.get_item(
                Key={'id': 'generic', 'time2400': time2400_value}
            )
            current_item = json_util.loads(current_item_json)['Item']
            # print(current_item)

            # Not doing any error catching here, but for this purpose there isn't really a need
            # We're very unlikely to have two simultaneous updates since this comes through
            # a queue
            summary_table.update_item(
                Key={'id': 'generic', 'time2400': time2400_value},
                UpdateExpression='ADD visits :incr',
                ExpressionAttributeValues={':incr': 1},
                ConditionExpression=Attr('visits').eq(current_item['visits'])
            )
        except BaseException as e:
            print("RECORD_NOT_PROCESSED")
            print(f"Exception object: {e}")
            print(f"Stream record: {record_json}")
